import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public showKeyboard = false;
  codeArray: number[] = [];
  code: Element[] = [];
  constructor(public navCtrl: NavController, public storage: Storage) {
  }

  ionViewWillEnter(){
    this.storage.get("code").then(element => {
      if(element !== null){
        this.navCtrl.push(TabsPage);
      }
    })
  }
  enterCode(code: Element) {
    const img: Element = document.querySelector(".img-logo");

    for (let i = 1; i <= code.children.length; i++) {
      this.code.push(code.children.item(i));
    }

    if (img !== null){
      img.remove();
      this.showKeyboard = true;
    } else {
      this.showKeyboard = true;
    }
  }

  enterNumbers(element: any){
    if (parseInt(element.target.textContent) || parseInt(element.target.textContent) === 0) {
      let num:number = parseInt(element.target.textContent);

      if (this.codeArray.length <= 4){
        this.addNum(num);
        if (this.codeArray.length === 5) this.login();
      }

    } else {
      this.deleteNum();
    }
  }

  private addNum(num: number) {
    this.codeArray.push(num);
    for (let i = 0; i < this.codeArray.length; i++) {
      this.code[i].textContent = this.codeArray[i].toString();
    }
  }

  private deleteNum(){
    this.codeArray.pop();
    for (let i = 0; i < 5; i++) {
      this.code[i].textContent = this.codeArray[i] === undefined ? "" : this.codeArray[i].toString();
    }
  }

  private login() {
    this.storage.set("code", this.codeArray)
    this.codeArray = [];
    for (let i = 0; i < 5; i++) {
      this.code[i].textContent = this.codeArray[i] === undefined ? "" : this.codeArray[i].toString();
    }
    this.navCtrl.push(TabsPage);
  }

}

