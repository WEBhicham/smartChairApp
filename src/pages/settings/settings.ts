import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})

export class SettingsPage {
  constructor(public navCtrl: NavController, public storage: Storage, public app: App) {
  }

  logout(){
    this.storage.remove("code");
    let nav = this.app.getRootNav();
    nav.setRoot(LoginPage);
    this.navCtrl.popToRoot();
  }


  reset(){
    this.storage.remove("settings");
    this.navCtrl.parent.select(1);
  }
}
