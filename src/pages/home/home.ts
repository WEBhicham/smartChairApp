import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController } from 'ionic-angular';
import { Settings } from '../../interfaces/settings.interface';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  code : string;
  formGroup: FormGroup;
  name: string = "standard";
  settingsHTML: string = "";
  armrestHeight: number = 0;
  seatHeight: number = 0;
  backAngle: number = 0;
  legSupport: number = 0;
  upperMassageIntensity: string = "low";
  lowerMassageIntensity: string = "low";
  leggMassageIntensity: string = "low";
  screen: boolean = false;
  brightness: number = 0;
  contract: number = 0;
  sharpness: number = 0;
  gamma: number = 0;
  upperTemp: number = 0;
  lowerTemp: number = 0;
  leggTemp: number = 0;
  globalVolume: number = 0;
  trebble: number = 0;
  bassIntensity: number = 0;
  currentSetting: string = "standard";
  settingsArray: Settings[] = [];
  constructor(public navCtrl: NavController,public storage: Storage) {
    this.formGroup = new FormGroup({
      name: new FormControl()
    });
  }

  ionViewDidEnter(){
    const settingsStandard: Settings = {
      name: this.name,
      armrestHeight: this.armrestHeight,
      seatHeight: this.seatHeight,
      backAngle: this.backAngle,
      legSupport: this.legSupport,
      upperMassageIntensity: this.upperMassageIntensity,
      lowerMassageIntensity: this.lowerMassageIntensity,
      leggMassageIntensity: this.leggMassageIntensity,
      screen: this.screen,
      brightness: this.brightness,
      contract: this.contract,
      sharpness: this.sharpness,
      gamma: this.gamma,
      upperTemp: this.upperTemp,
      lowerTemp: this.lowerTemp,
      leggTemp: this.leggTemp,
      globalVolume: this.globalVolume,
      trebble: this.trebble,
      bassIntensity: this.bassIntensity
    }

      this.storage.get("settings").then(settings => {
        if(settings){
          settings.forEach(setting => {
            this.settingsArray.push(setting);
          });
        }else {
          this.settingsArray = [];
          this.storage.set("settings", [settingsStandard]);
          this.settingsArray.push(settingsStandard);
        }
      })
    }

  switch(event: Event){
    const setting = (event.currentTarget as HTMLElement).nextElementSibling as HTMLElement;
    const dot = (event.currentTarget as HTMLElement).children[0] as HTMLElement;
    if(setting.style.display === "block") {
      setting.style.display = "none";
      this.switchClass(dot, "point-close", "point-open");
    } else {
      setting.style.display = "block";
      this.switchClass(dot, "point-open", "point-close");
    }
  }

  private switchClass(element: HTMLElement, class1: string, class2: string){
    if(element.classList.contains(class1)) {
      element.classList.remove(class1);
      element.classList.add(class2);
    } else {
      element.classList.remove(class2);
      element.classList.add(class1);
    }
  }

  saveSetting(formValues: {name: string}){
    const settings: Settings = {
      name: formValues.name,
      armrestHeight: this.armrestHeight,
      seatHeight: this.seatHeight,
      backAngle: this.backAngle,
      legSupport: this.legSupport,
      upperMassageIntensity: this.upperMassageIntensity,
      lowerMassageIntensity: this.lowerMassageIntensity,
      leggMassageIntensity: this.leggMassageIntensity,
      screen: this.screen,
      brightness: this.brightness,
      contract: this.contract,
      sharpness: this.sharpness,
      gamma: this.gamma,
      upperTemp: this.upperTemp,
      lowerTemp: this.lowerTemp,
      leggTemp: this.leggTemp,
      globalVolume: this.globalVolume,
      trebble: this.trebble,
      bassIntensity: this.bassIntensity
    }
    this.settingsArray.push(settings);
    this.storage.set("settings", this.settingsArray).then(e => {
      this.updateSettings();
    });


    const modal = (document.querySelector(".modal") as HTMLElement);
    modal.style.visibility = "hidden";
  }

  showModal(){
    const modal = (document.querySelector(".modal") as HTMLElement);
    modal.style.visibility = "visible";
  }

  updateSettings(){
    this.settingsArray = [];
    this.storage.get("settings").then(settings => {
      settings.forEach((setting: Settings) => {
        this.settingsArray.push(setting);
      })
    });
  }

  update(){
    this.storage.get("settings").then(settings => {
      settings.forEach((setting: Settings )=> {
        if(setting.name === this.currentSetting) {
          this.name = setting.name,
          this.armrestHeight = setting.armrestHeight,
          this.seatHeight = setting.seatHeight,
          this.backAngle = setting.backAngle,
          this.legSupport = setting.legSupport,
          this.upperMassageIntensity = setting.upperMassageIntensity,
          this.lowerMassageIntensity = setting.lowerMassageIntensity,
          this.leggMassageIntensity = setting.leggMassageIntensity,
          this.screen = setting.screen,
          this.brightness = setting.brightness,
          this.contract = setting.contract,
          this.sharpness = setting.sharpness,
          this.gamma = setting.gamma,
          this.upperTemp = setting.upperTemp,
          this.lowerTemp = setting.lowerTemp,
          this.leggTemp = setting.leggTemp,
          this.globalVolume = setting.globalVolume,
          this.trebble = setting.trebble,
          this.bassIntensity = setting.bassIntensity
        }
      })
    });
  }
}
