import { Component } from '@angular/core';

import { SettingsPage } from '../settings/settings';
import { HomePage } from '../home/home';
import { InfoPage } from '../info/info';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = SettingsPage;
  tab2Root = HomePage;
  tab3Root = InfoPage;
  rootPage = LoginPage;

  constructor(public storage: Storage) {

  }
}
