export interface Settings {
  name: string;
  armrestHeight: number;
  seatHeight: number;
  backAngle: number;
  legSupport: number;
  upperMassageIntensity: string;
  lowerMassageIntensity: string;
  leggMassageIntensity: string;
  screen: boolean;
  brightness: number;
  contract: number;
  sharpness: number;
  gamma: number;
  upperTemp: number;
  lowerTemp: number;
  leggTemp: number;
  globalVolume: number;
  trebble: number;
  bassIntensity: number;
}
